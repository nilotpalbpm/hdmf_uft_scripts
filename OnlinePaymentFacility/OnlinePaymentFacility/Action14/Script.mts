﻿RunAction "Amount Due", oneIteration, Parameter("in_startMonth"), Parameter("in_endMonth"), Parameter("in_savingsAmount"), Parameter("amountDue"), Parameter("numMonthsCovered")
RunAction "Convenience Fee", oneIteration, Parameter("Amount Due", "numMonthsCovered"), Parameter("Amount Due", "amountDue"), Parameter("convenienceFee")
RunAction "Total Amount Due", oneIteration, Parameter("Amount Due", "amountDue"), Parameter("Convenience Fee", "convenienceFee"), Parameter("totalAmountDue")
