﻿Dim thisBrowser, thisPage, ctlMonthStart, ctlYearStart
Dim selectionDate, selMonth, selYear
Dim months(12)

'Initialize the month array
months(0) = "JANUARY"
months(1) = "FEBRUARY"
months(2) = "MARCH"
months(3) = "APRIL"
months(4) = "MAY"
months(5) = "JUNE"
months(6) = "JULY"
months(7) = "AUGUST"
months(8) = "SEPTEMBER"
months(9) = "OCTOBER"
months(10) = "NOVEMBER"
months(11) = "DECEMBER"

Set thisBrowser = Browser("Pag-IBIG Fund Payment")
Set thisPage = thisBrowser.Page("Pag-IBIG Fund Payment")
Set ctlMonthStart = thisPage.WebList("ddlMonthStart")
Set ctlYearStart = thisPage.WebList("ddlYearStart")

'Take a date 2 months prior to current one
selectionDate = DateAdd("m", -2, Date)
selMonth = DatePart("m", selectionDate)
selYear = DatePart("yyyy", selectionDate)

ctlMonthStart.Select months(selMonth - 1)
ctlYearStart.Select CStr(selYear)
 @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlMonthStart")_;_script infofile_;_ZIP::ssf1.xml_;_
thisPage.WebElement("INVALID SELECTION OF PERIOD").Check CheckPoint("INVALID SELECTION OF PERIOD COVERED") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("INVALID SELECTION OF PERIOD")_;_script infofile_;_ZIP::ssf2.xml_;_
thisPage.WebButton("Ok").Click
