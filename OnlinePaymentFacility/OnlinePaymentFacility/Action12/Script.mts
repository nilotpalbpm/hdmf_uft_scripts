﻿Dim thisBrowser, thisPage, ctlMonthStart, ctlYearStart
Dim startDate, selectionDate, selMonth, selYear
Dim months(12)

'Initialize the month array
months(0) = "JANUARY"
months(1) = "FEBRUARY"
months(2) = "MARCH"
months(3) = "APRIL"
months(4) = "MAY"
months(5) = "JUNE"
months(6) = "JULY"
months(7) = "AUGUST"
months(8) = "SEPTEMBER"
months(9) = "OCTOBER"
months(10) = "NOVEMBER"
months(11) = "DECEMBER"

Set thisBrowser = Browser("Pag-IBIG Fund Payment")
Set thisPage = thisBrowser.Page("Pag-IBIG Fund Payment")
Set ctlMonthStart = thisPage.WebList("ddlMonthEnd")
Set ctlYearStart = thisPage.WebList("ddlYearEnd")

'Take a date 1 month prior to start date
startDate = Parameter.Item("startMonth")
selectionDate = DateAdd("m", -1, startDate)
selMonth = DatePart("m", selectionDate)
selYear = DatePart("yyyy", selectionDate)

ctlMonthStart.Select months(selMonth - 1)
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlYearEnd").Select CStr(selYear)
'ctlYearStart.Select CStr(selYear)

thisPage.WebElement("INVALID SELECTION OF PERIOD").Check CheckPoint("INVALID SELECTION OF PERIOD COVERED")
thisPage.WebButton("Ok").Click @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebButton("Ok")_;_script infofile_;_ZIP::ssf3.xml_;_
