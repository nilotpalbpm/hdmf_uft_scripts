﻿Dim accNum, accName

Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment_2").WebButton("Proceed").Click

Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("PayType").Check CheckPoint("PayType")
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMembershipType").Check CheckPoint("txtMembershipType") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMembershipType")_;_script infofile_;_ZIP::ssf2.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("AccountNo").Check CheckPoint("AccountNo") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("AccountNo")_;_script infofile_;_ZIP::ssf3.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("AccountName").Check CheckPoint("AccountName") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("AccountName")_;_script infofile_;_ZIP::ssf4.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN").Check CheckPoint("txtMPN") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN")_;_script infofile_;_ZIP::ssf5.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtEmail").Check CheckPoint("txtEmail") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtEmail")_;_script infofile_;_ZIP::ssf6.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("transactionamount").Check CheckPoint("transactionamount") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("transactionamount")_;_script infofile_;_ZIP::ssf7.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("Per").Check CheckPoint("Per") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("Per")_;_script infofile_;_ZIP::ssf8.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("ConvenienceFee").Check CheckPoint("ConvenienceFee") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("ConvenienceFee")_;_script infofile_;_ZIP::ssf9.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("amount").Check CheckPoint("amount") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("amount")_;_script infofile_;_ZIP::ssf10.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("orderRef").Check CheckPoint("orderRef") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("orderRef")_;_script infofile_;_ZIP::ssf11.xml_;_

accNum = Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("AccountNo").GetROProperty("Value")
accName = Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("AccountName").GetROProperty("Value")

If CStr(Parameter("memberRTN")) <> accNum Then
	Reporter.ReportEvent micFail, "Payment Summary", "RTN did not match" & vbCrLf & "Expected: " & Parameter("memberRTN") & " Got: " & accNum
End If

If Parameter("memberName") <> accName Then
	Reporter.ReportEvent micFail, "Payment Summary", "Member name did not match"
End If
