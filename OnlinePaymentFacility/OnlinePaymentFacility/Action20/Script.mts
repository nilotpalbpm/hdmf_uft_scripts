﻿Dim totalAmount, capturedAmount, amountDue, convenienceFee

amountDue = CCur(Parameter("amountDue"))
convenienceFee = CCur(Parameter("convenienceFee"))

totalAmount = amountDue + convenienceFee

capturedAmount = Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtTotal").GetROProperty("Value")

Parameter("totalAmountDue") = totalAmount

totalAmount = CCur(totalAmount)
capturedAmount = CCur(capturedAmount)

If capturedAmount = totalAmount Then
	Reporter.ReportEvent micPass, "Total Amount", "Total amount calculated correctly"
else
	Reporter.ReportEvent micFail, "Total Amount", "Total amount calculation yielded incorrect results" & vbCrLf & _
	"Expected: " & totalAmount & vbCrLf & "Got: " & capturedAmount
End If
