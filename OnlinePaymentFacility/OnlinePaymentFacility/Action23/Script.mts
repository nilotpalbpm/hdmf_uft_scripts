﻿Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebCheckBox("chkSMS").Set "ON"

Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebCheckBox("chkSMS").Check CheckPoint("chkSMS") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebCheckBox("chkSMS")_;_script infofile_;_ZIP::ssf1.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlNetwork").Check CheckPoint("ddlNetwork") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlNetwork")_;_script infofile_;_ZIP::ssf2.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlPrefix").Check CheckPoint("ddlPrefix") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlPrefix")_;_script infofile_;_ZIP::ssf3.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN").Check CheckPoint("txtMPN")

Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlNetwork").Select "Select Network" @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("Network is required.")_;_script infofile_;_ZIP::ssf12.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlPrefix").Select "Select Prefix" @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlPrefix")_;_script infofile_;_ZIP::ssf5.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN").Set "" @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN")_;_script infofile_;_ZIP::ssf6.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebButton("Proceed").Click @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebButton("Proceed")_;_script infofile_;_ZIP::ssf7.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("Network is required.").Check CheckPoint("Network is required.")
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("Prefix is required.").Check CheckPoint("Prefix is required.") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("Prefix is required.")_;_script infofile_;_ZIP::ssf8.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("Number is required.").Check CheckPoint("Number is required.") @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebElement("Number is required.")_;_script infofile_;_ZIP::ssf9.xml_;_

'Set correct values

Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlNetwork").Select "SMART" @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlNetwork")_;_script infofile_;_ZIP::ssf13.xml_;_
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebList("ddlPrefix").Select "0919"
Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN").Set "9717851" @@ hightlight id_;_Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtMPN")_;_script infofile_;_ZIP::ssf4.xml_;_
