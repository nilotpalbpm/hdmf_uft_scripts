﻿Dim thisBrowser, thisPage
Dim amount, monthStart, monthEnd, monthsCovered, amountDue, capturedAmount

Set thisBrowser = Browser("Pag-IBIG Fund Payment")
Set thisPage = thisBrowser.Page("Pag-IBIG Fund Payment")

monthStart = Parameter("startMonth")
monthEnd = Parameter("endMonth")

monthsCovered = DateDiff("m", monthStart, monthEnd)

'Amount due
amount = thisPage.WebEdit("txtAmount").GetROProperty("Value")
amountDue = CCur(amount) * monthsCovered

'Store parameter values for other steps to use
Parameter("amountDue") = amountDue
Parameter("numMonthsCovered") = monthsCovered

capturedAmount = thisPage.WebEdit("txtAmountdue").GetROProperty("Value")

capturedAmount = CCur(capturedAmount)

If capturedAmount = amountDue Then
	Reporter.ReportEvent micPass, "Amount Calculation", "Due amount was calculated correctly"
else
	Reporter.ReportEvent micFail, "Amount Calculation", "Due amount calculation yields unexpected result." & vbCrLf & "Expected: " & amountDue & vbCrLf & "Got: " & capturedAmount
End If
