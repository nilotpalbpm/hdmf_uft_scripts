﻿Dim pcConvenienceFee, totalConvenienceFee, amountDue, capturedFee

pcConvenienceFee = 3.6
amountDue = Parameter.Item("amountDue")

totalConvenienceFee = amountDue * (pcConvenienceFee / 100)

capturedFee = Browser("Pag-IBIG Fund Payment").Page("Pag-IBIG Fund Payment").WebEdit("txtConvenienceFee").GetROProperty("Value")

capturedFee = CCur(capturedFee)

Parameter("convenienceFee") = totalConvenienceFee

If capturedFee = totalConvenienceFee Then
	Reporter.ReportEvent micPass, "Convenience Fee Calculation", "Fee calculated successfully at " & pcConvenienceFee & "%"
else
	Reporter.ReportEvent micFail, "Convenience Fee Calculation", "Fee calculation failed." & vbCrLf & "Expected: " & totalConvenienceFee & vbCrLf & "Got: " & capturedFee
End If
