﻿RunAction "Launch Application", oneIteration
RunAction "Check Required Fields", oneIteration
RunAction "Populate Required Fields", oneIteration
RunAction "RTN Verification", oneIteration
RunAction "Monthly Mandatory Savings", oneIteration
RunAction "Period Covered", oneIteration
RunAction "Amount Calculation", oneIteration, Parameter("Period Covered", "out_startMonth"), Parameter("Period Covered", "out_endMonth"), Parameter("Monthly Mandatory Savings", "savingsAmount")
RunAction "Notifications", oneIteration
RunAction "Agreement Verification", oneIteration
RunAction "Payment Summary", oneIteration, Parameter("RTN Verification", "memberRTN"), Parameter("RTN Verification", "memberName"), Parameter("Amount Calculation", "amountDue"), Parameter("Period Covered", "out_startMonth"), Parameter("Period Covered", "out_endMonth"), Parameter("Amount Calculation", "convenienceFee"), Parameter("Amount Calculation", "totalAmountDue")
